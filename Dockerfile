# EPICS 7 - Ubuntu 22.04 Base Dockerfile

##### shared environment stage #################################################

FROM ubuntu:22.04 AS base

ENV EPICS_HOST_ARCH=linux-x86_64
ENV EPICS_ROOT=/opt/epics
ENV EPICS_BASE=${EPICS_ROOT}/base
ENV LD_LIBRARY_PATH=${EPICS_BASE}/lib/${EPICS_HOST_ARCH}
ENV VIRTUALENV /venv
ENV PATH=${VIRTUALENV}/bin:${EPICS_BASE}/bin/${EPICS_HOST_ARCH}:${PATH}
ENV SUPPORT ${EPICS_ROOT}/support
ENV IOC ${EPICS_ROOT}/ioc
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Berlin

##### developer / build stage ##################################################


# install build tools and utilities
RUN apt-get update -y && apt-get upgrade -y && \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    libntirpc-dev libtirpc3 \
    tzdata \
    build-essential \
    busybox \
    git \
    python3-minimal \
    python3-pip \
    python3-venv \
    re2c \
    rsync \
    ssh-client \
    && rm -rf /var/lib/apt/lists/* \
    && busybox --install
## clean the apt's cache
RUN apt-get clean \
    && rm -rf /var/cache/apt \
## create download repository
RUN mkdir /opt/src \
    && mkdir /opt/epics \
    && mkdir /opt/epics/base


## setting the timezone
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone  
# Clone the conf files into the docker container
RUN git clone --depth 1 --recursive --branch R7.0.7 https://github.com/epics-base/epics-base.git /opt/src
# Specify the location of the installation
RUN echo "INSTALL_LOCATION"=${EPICS_BASE} > /opt/src/configure/CONFIG_SITE.local

RUN make -C /opt/src -j $(nproc) \
    && rm -rf /opt/src